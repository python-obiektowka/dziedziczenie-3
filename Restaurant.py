class Restaurant:
    def __init__(self, restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type

    def describe_restaurant(self):
        print(f"Name: {self.restaurant_name}\nCuisine type: {self.cuisine_type}")

    def open_restaurant(self):
        print("Godziny otwarcia: 8:00-16:00")


class IceCreamStand(Restaurant):
    flavors = ["zielone", "niebieskie", "czerwone"]

    def __init__(self, restaurant_name, cuisine_type):
        super().__init__(restaurant_name, cuisine_type)

    def show_flavors(self):
        print(f"Flavors: {', '.join(self.flavors)}")


restaurant = Restaurant("asdad", "Asdasd")
print(restaurant.restaurant_name + "    " + restaurant.cuisine_type)
restaurant.describe_restaurant()
restaurant.open_restaurant()

ice = IceCreamStand("asdsad", "asdadsa")
ice.show_flavors()
